# $Id: $

This module is core functionality for platnosci.pl payment gateway.
Module partly uses code from module created by ad4m for ubercart.

It contains base functions and vocabularies for platnosci.pl gateway.
It doesn't do anything on its own. It provides only gateway transactions, without orders.
E-commerce modules have to create their own checkout flow.

How to use this module in your own module:
1. Create new transaction
2. Fill transaction with data
3. When ready get form for checkout.
4. Add hooks for transaction state change

