<?php
// $Id: $

define('UTF_ENC', 1);
define('ISO_ENC', 2);
define('WIN_ENC', 3);

define('XML_FORMAT', 1);
define('TXT_FORMAT', 2);

define('PAPI_E_MISSING_POS_ID',		100);	// brak parametru pos id
define('PAPI_E_MISSING_SESS_ID',		101);	// brak parametru session id
define('PAPI_E_MISSING_TS',			102);	// brak parametru ts
define('PAPI_E_MISSING_SIG',			103);	// brak parametru sig
define('PAPI_E_MISSING_DESC',			104);	// brak parametru desc
define('PAPI_E_MISSING_CLIENT_IP',	105);	// brak parametru client ip
define('PAPI_E_MISSING_FIRSTNAME',	106);	// brak parametru first name
define('PAPI_E_MISSING_LASTNAME',	107);	// brak parametru last name
define('PAPI_E_MISSING_STREET',		108);	// brak parametru street
define('PAPI_E_MISSING_CITY',			109);	// brak parametru city
define('PAPI_E_MISSING_POSTCODE',	110);	// brak parametru post code
define('PAPI_E_MISSING_AMOUNT',		111);	// brak parametru amount
define('PAPI_E_INVALID_ACCOUNT_NUM',112);	// błedny numer konta bankowego
define('PAPI_E_MISSING_EMAIL',		113);	// brak parametru email
define('PAPI_E_MISSING_PHONE',		114);	// brak numeru telefonu
define('PAPI_E_OTHER_TEMP_ERROR',	200);	// inny chwilowy bład
define('PAPI_E_OTHER_TEMP_DB_ERROR',201);	// inny chwilowy bład bazy danych
define('PAPI_E_POS_LOCKED',			202);	// Pos o podanym identyfikatorze jest zablokowany
define('PAPI_E_INVALID_PAYTYPE',		203);	// niedozwolona wartosc pay type dla danego pos id
define('PAPI_E_PAYTYPE_TEMP_LOCK',	204);	// podana metoda płatnosci (wartosc pay type) jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej
define('PAPI_E_AMOUNT_BELOW_MIN',	205);	// kwota transakcji mniejsza od wartosci minimalnej
define('PAPI_E_AMOUNT_EXCEEDED_MAX',206);	// kwota transakcji wieksza od wartosci maksymalnej
define('PAPI_E_TOTAL_AMOUNT_EXCEEDED',207);	// przekroczona wartosc wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym
define('PAPI_E_EXPRESSPAY_INACTIVE',208);	// Pos działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)
define('PAPI_E_INVALID_POS_KEY', 209); //błedny numer pos_id lub pos_auth_key
define('PAPI_E_NO_SUCH_TRANSACTION',500);	// transakcja nie istnieje
define('PAPI_E_NO_AUTHORIZATION',	501);	// brak autoryzacji dla danej transakcji
define('PAPI_E_ALREADY_STARTED',		502);	// transakcja rozpoczeta wczesniej
define('PAPI_E_ALREADY_AUTHORIZED',	503);	// autoryzacja do transakcji była juz przeprowadzana
define('PAPI_E_ALREADY_CANCELLED',	504);	// transakcja anulowana wczesniej
define('PAPI_E_ALREADY_SENTFORRECV',505);	// transakcja przekazana do odbioru wczesniej
define('PAPI_E_ALREADY_RECIEVED',	506);	// transakcja juz odebrana
define('PAPI_E_MONEY_RETURN_ERROR',	507);	// bład podczas zwrotu srodków do klienta
define('PAPI_E_TRANSACTION_ERROR',	599);	// błedny stan transakcji, np. nie mozna uznac transakcji kilka razy lub inny, prosimy o kontakt
define('PAPI_E_CRITICAL',				999);	// inny bład krytyczny - prosimy o kontakt

/**
 * Translates error codes to error messages.
 *
 * @param int $error error code
 * @return string error message
 */
//TODO: change messages into translate able messages with t()
function _platnosci_api_getErrorDesc($error){
	switch($error){
		case PAPI_E_MISSING_POS_ID:			return "brak parametru pos id";
		case PAPI_E_MISSING_SESS_ID:			return "brak parametru session id";
		case PAPI_E_MISSING_TS:					return "brak parametru ts";
		case PAPI_E_MISSING_SIG:				return "brak parametru sig";
		case PAPI_E_MISSING_DESC:				return "brak parametru desc";
		case PAPI_E_MISSING_CLIENT_IP:		return "brak parametru client ip";
		case PAPI_E_MISSING_FIRSTNAME:		return "brak parametru first name";
		case PAPI_E_MISSING_LASTNAME:			return "brak parametru last name";
		case PAPI_E_MISSING_STREET:			return "brak parametru street";
		case PAPI_E_MISSING_CITY:				return "brak parametru city";
		case PAPI_E_MISSING_POSTCODE:			return "brak parametru post code";
		case PAPI_E_MISSING_AMOUNT:			return "brak parametru amount";
		case PAPI_E_INVALID_ACCOUNT_NUM:		return "błedny numer konta bankowego";
		case PAPI_E_MISSING_EMAIL:				return "brak parametru email";
		case PAPI_E_MISSING_PHONE:				return "brak numeru telefonu";
		case PAPI_E_OTHER_TEMP_ERROR:			return "inny chwilowy bład";
		case PAPI_E_OTHER_TEMP_DB_ERROR:		return "inny chwilowy bład bazy danych";
		case PAPI_E_POS_LOCKED:					return "Pos o podanym identyfikatorze jest zablokowany";
		case PAPI_E_INVALID_PAYTYPE:			return "niedozwolona wartosc pay type dla danego pos id";
		case PAPI_E_PAYTYPE_TEMP_LOCK:		return "podana metoda płatnosci (wartosc pay type) jest chwilowo zablokowana dla danego pos id, np. przerwa konserwacyjna bramki płatniczej";
		case PAPI_E_AMOUNT_BELOW_MIN:			return "kwota transakcji mniejsza od wartosci minimalnej";
		case PAPI_E_AMOUNT_EXCEEDED_MAX:		return "kwota transakcji wieksza od wartosci maksymalnej";
		case PAPI_E_TOTAL_AMOUNT_EXCEEDED:	return "przekroczona wartosc wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym";
		case PAPI_E_EXPRESSPAY_INACTIVE:		return "Pos działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)";
    case PAPI_E_INVALID_POS_KEY:     return "Błedny numer pos_id lub pos_auth_key";
		case PAPI_E_NO_SUCH_TRANSACTION:		return "transakcja nie istnieje";
		case PAPI_E_NO_AUTHORIZATION:			return "brak autoryzacji dla danej transakcji";
		case PAPI_E_ALREADY_STARTED:			return "transakcja rozpoczeta wczesniej";
		case PAPI_E_ALREADY_AUTHORIZED:		return "autoryzacja do transakcji była juz przeprowadzana";
		case PAPI_E_ALREADY_CANCELLED:		return "transakcja anulowana wczesniej";
		case PAPI_E_ALREADY_SENTFORRECV:		return "transakcja przekazana do odbioru wczesniej";
		case PAPI_E_ALREADY_RECIEVED:			return "transakcja juz odebrana";
		case PAPI_E_MONEY_RETURN_ERROR:		return "bład podczas zwrotu srodków do klienta";
		case PAPI_E_TRANSACTION_ERROR:		return "błedny stan transakcji, np. nie mozna uznac transakcji kilka razy lub inny, prosimy o kontakt";
		case PAPI_E_CRITICAL:					return "inny bład krytyczny - prosimy o kontakt";
		default:									return t('Error %error: no such error',array('%error' => $error));
	}
}

/* Note: 'PAPI_TS' stands for 'platnosci.pl Transaction State' */
define('PAPI_TS_ZERO',			  0);	// status zerowy - domyślny status używany dla nowo utworzonych wpisów.
define('PAPI_TS_NEW',			  1);	// nowa
define('PAPI_TS_CANCELLED',	  2);	// anulowana
define('PAPI_TS_REJECTED',		  3);	// odrzucona
define('PAPI_TS_PENDING',		  4); // rozpoczeta
define('PAPI_TS_AWAITING_RECV', 5);	// oczekuje na odbiór
define('PAPI_TS_PAYMENT_REJECTED',7);	// płatnosc odrzucona, otrzymano srodki ok klienta po wczesniejszym anulowaniu transakcji, lub nie było mozliwosci zwrotu srodków w sposób automatyczny, sytuacje takie beda monitorowane i wyjasniane przez zespół Płatnosci
define('PAPI_TS_FINALIZED',	 99);	// płatnosc odebrana - zakonczona
define('PAPI_TS_ERROR',			888); // błedny status - prosimy o kontakt

/* Note: 'PAPI_P' stands for 'platnosci.pl parameter' */
define('PAPI_P_POS_ID',			'pos_id');
define('PAPI_P_SESSION_ID',		'session_id');
define('PAPI_P_TS',				'ts');
define('PAPI_P_SIGNATURE',		'sig');
define('PAPI_P_TRANSACTION_ID', 'trans_id');
define('PAPI_P_TRANSACTION_STATUS', 'transaction_status');
define('PAPI_P_PAYMENT_TYPE',	'pay_type');
define('PAPI_P_AMOUNT',			'amount');
define('PAPI_P_IP',				'client_ip');
define('PAPI_P_FIRSTNAME',		'first_name');
define('PAPI_P_LASTNAME',		'last_name');
define('PAPI_P_EMAIL',			'email');
define('PAPI_P_DESCRIPTION',	'desc');
define('PAPI_P_AMOUNT_PS',		'amount_ps');
define('PAPI_P_AMOUNT_CS',		'amount_cs');
define('PAPI_P_ORDER_ID',		'order_id');
define('PAPI_P_CREATED',			'create_time');
define('PAPI_P_LAST_UPDATE',	'last_update');
define('PAPI_P_USER_ID',			'user_id');
define('PAPI_P_ID',				'id');
define('PAPI_P_STATUS',			'status');
define('PAPI_P_ERROR',			'error');

/* Note: 'PAPI_PR' stands for 'platnosci.pl procedure' */
define('PAPI_PR_NEW',		'NewPayment');
define('PAPI_PR_NEW_SMS',	'NewSMS');
define('PAPI_PR_GET',		'Payment/get');
define('PAPI_PR_CONFIRM', 'Payment/confirm');
define('PAPI_PR_CANCEL',	'Payment/cancel');


