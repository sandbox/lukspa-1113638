<?php
// $Id: $

/**
* Counts and checks signature for provided arguments. Param number and order is important.
* 
* @param string arg1, arg2, arg2, 
* @return boolean 
*/
function platnosci_api_validate_sig(){
	$args = func_get_args();
	$sig = array_shift($args);
	$sig_confirm = '';

	foreach($args as $arg){
		$sig_confirm.= $arg;
	}

	if(md5($sig_confirm) == $sig){
		return true;
	}
	watchdog('platnosci api', 'Signature did not pass validation', array(), WATCHDOG_ERROR );
	return false;
}

/**
* Creates url for specified payment procedure
*
* @param string name of procedure
* @return string url
*/
function platnosci_api_create_url($procedure){
	$url = variable_get('platnosci_api_url','https://www.platnosci.pl/paygw/')
		.platnosci_api_data_enc(variable_get('platnosci_api_data_enc', UTF_ENC)).'/'.$procedure;

        // adds return data format value to url end
	$url .= $procedure == (PAPI_PR_NEW || PAPI_PR_NEW_SMS) ? '' : '/'.platnosci_api_data_format(variable_get('platnosci_api_data_format', XML_FORMAT));

	return $url;
}

/**
* Reads data from session
*
* @param string key
* @return mixed 
*/
function platnosci_api_sess_read($key){
  $session = db_fetch_object(db_query("SELECT s.* FROM {sessions} s WHERE s.sid = '%s'", $key));
	if($session){
		session_decode($session->session);
    return $session;
	}
	return FALSE;
}

/**
* Gets encodiing type code
*
* @param int type
* @return string encoding code
*/
function platnosci_api_data_enc($type = null){
	$enc_type = array(
		UTF_ENC => 'UTF',
		ISO_ENC => 'ISO',
		WIN_ENC => 'WIN'
	);
	if($type !== null){
		return $enc_type[$type];
	}
	return $enc_type;
}

/**
* Gets data format code
*
* @param int format
* @return string code
*/
function platnosci_api_data_format($format = null){
	$format_type = array(
		XML_FORMAT => 'xml',
		TXT_FORMAT => 'txt'
	);
	if($format !== null){
		return $format_type[$format];
	}
	return $format_type;
}

/**
* Makes http request to payment getaway
*
* @param string url
* @param array parameters
* @param boolean use ssl
* @return array response
*/
function platnosci_api_request($url, $data = array(), $ssl = false){
	$dataStr = '';

	if(!empty($data)){
		foreach($data as $key=>$value){
			$dataStr .= $key.'='.$value.'&';
		}
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl);
	curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);

	$response = curl_exec($ch);
        $error = curl_error($ch);
	if(!empty($error)){
		watchdog('platnosci_api', $error, array(), WATCHDOG_ERROR);
	}else{
		$content_type = array_shift(explode(';', curl_getinfo($ch, CURLINFO_CONTENT_TYPE)));
		if($content_type == 'text/xml'){
			$response = platnosci_api_parse_XML($response);
		}
	}
	curl_close($ch);

	return $response;
}

/**
* Converts xml response into data array
*
* @param string xml document
* @return array
*/
function platnosci_api_parse_XML($xml){

    $root = new SimpleXMLElement($xml);
    if (!$root instanceof SimpleXMLElement) {
        throw new Exception("Error parsing response");
    }

    $data = array();
    if (isset($root->response->trans)) {
        
        foreach ($root->response->trans->children() as $field) {

            // Entity API had problem with column named desc (?)
            // changed to description
            if ($field->getName() == 'desc') {
                $data['description'] = $field;
            } else {
                $data[$field->getName()] = $field;
            }
        }

        return $data;
    } else {
        throw new Exception("Invalid xml format of response");
    }

    
}

/**
* Returns error message for specified error code
* @param int error code
* @return string error message
*/
function platnosci_api_get_error_message($code){
    return _platnosci_api_getErrorDesc($code);
}
