<?php
// $Id:  $

define('DEFAULT_ENCODING', 'UTF-8');

class xmlParser {
	
	var $depth = 0;
	var $current_position = array();
	var $parents = array();
	var $parent_element = '';
	var $current_element = '';
	var $parent = null;
	var $current = null;
	
	var $response = array();
	
	var $error = false;
	var $error_message;
	
	var $xml_parser;
	var $xml;
	
	function xmlParser($xml, $enc = DEFAULT_ENCODING){
		if(!empty($xml)){
			$this->xml = $xml;
			$this->xml_parser = xml_parser_create($enc);
			xml_set_object($this->xml_parser, $this);
			xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, 0);
			xml_set_element_handler($this->xml_parser, 'startElement', 'endElement');
			xml_set_character_data_handler($this->xml_parser, 'characterData');
			if(!xml_parse($this->xml_parser, $this->xml, true)){
				$this->error = true;
				$this->error_message =  xml_error_string(xml_get_error_code($this->xml_parser));
			}
		}
	}
	
	function startElement($xml_parser, $tagName, $attr){
//		remeber last position on each level
		if(!isset($this->current_position[$this->depth]))
			$this->current_position[$this->depth] = 0;
			
		$position = $this->current_position[$this->depth];
			
//		set current and parent element name
		if(!empty($this->current_element))
			$this->parent_element = $this->current_element;
		$this->current_element = $tagName;
		$this->parent_elements[$this->depth] = $this->parent_element;
		
//		play with the reference so we can build structured array
		if(empty($this->parent_element)){
//			we are starting with setting reference to the array storing response
			$this->current = &$this->response;
		}else{
//			if we are further retrive current element
			$this->current = &$this->parent;
		}
//		remeber the parent on current depth
		$this->parents[$this->depth][$position] = &$this->parent;

//		here is the magic - we are filling response array and setting the parent element
		$this->current[$this->current_element][$position] = array();
		$this->parent = &$this->current[$this->current_element][$position];
		
//		prepare for the next stage
		$this->current_position[$this->depth]++;
		$this->depth++;
	}
	
	function characterData($xml_parser, $data){
//		if there is some data, store it
		if(trim($data)){
      if(is_array($this->current[$this->current_element]))
			  $this->current[$this->current_element] = $data;
      else
        $this->current[$this->current_element] .= $data;
    }
	}
	
	function endElement($xml_parser, $tagName){
		$this->depth--;
//		set current tag name one level up
		$this->current_element = $this->parent_mem[$this->depth];
//		set current element back to parent
		$position = $this->current_position[$this->depth] -1;
//		we are going level down, so prepare propper element as parent
		$this->parent = &$this->parents[$this->depth][$position];
	}
	
	function getResponse(){
		return $this->response;
	}
	
	function getErrors(){
		if($this->error){
			return $this->error_message;
		}
		return false;
	}
}
